## Notice

This is a fork of hgarcia, but his repository seems inactive, so I published this fixed version on npm under the name **bitbucket-api2**

## Intro

A very simple to use library to hook into the BitBucket api.
At the moment it covers issues and all related objects as well as getting repository and sources information.
The Api is in flux and you should expect changes coming in the future.

### Callbacks

All callbacks take the standard node signature of `function (error, result)` unless otherwise specified.

### CRUD API

Some object expose a CRUD api that consist of the following methods and signature.

    create(obj, cb);
    getAll(cb);
    getById(id, cb);
    update(id, obj, cb);
    delete(id, cb(err));

The callback for the `delete` method only returns an error or null if succesful.

The `obj` argument can be either a literal representing the resource or (in some cases) just a primitive, (eg: `Milestones` takes a `string`).

Specific details or changes to this API will be indicated in each object if need it.

### Usage

    var bitbucket = require('bitbucket-api');
    var credentials = {username: 'USER', password: 'PWD'};
    var client = bitbucket.createClient(credentials);
    var repository = client.getRepository({slug: 'SLUG', owner: 'OWNER'}, function (err, repo) {
       //Code to access the repo object.
    });

## Running the tests.

    npm install
    cd tests/
    mv helper.js.back helper.js
    vim helper.js //Enter your BitBucket credentials and a changeset on your repo and save the file
    cd ../
    npm test

All the tests at the moment are integration tests and will attempt to connect with BitBucket.
False errors may happen due to timeout scenarios but it should be fine in most cases.
They run with a 15 secs timeout.

If you want to run the tests with a different timeout run then directly with mocha and the -t option, giving it the timeout in milliseconds.

    mocha /tests -t 15000

## Changelog
=======

#### 0.0.6

* Adding support for user endpoint.
* Also adding support for users endpoint. At the moment only account and emails resources have been implemented.
* Breaking change, the cb in all .remove() methods always return null in the results.
* Improved and simplified documentation.

#### 0.0.5

* Adding support for changesets and changeset.
* Adding support for followers.
* Complete documentation for Issues, Issue, Issue.Comments, Changeset, Changesets and Followers

#### 0.0.4

* Improve code structure and prepare to support multiple connection transports
* Improve API: removing {succes: false} on error situations and returning Errors in the callback

#### 0.0.3

* Added support for sources

## Api

### createClient

Takes an object with a `username` and a `password` keys
Returns a BitBucket object

    var bitbucket = require('bitbucket-api');
    var credentials = {username: 'USER', password: 'PWD'};
    var client = bitbucket.createClient(credentials);


### BitBucket

#### Instance properties

    username // The username passed to the createClient function.
    password // The password passed to the createClient function.
    root     // The root for the bitbucket api, defaults to 'api.bitbucket.org'
    version  // The version for the bitbucket api, defaults to '1.0s'

#### Instance methods

    credentials() // returns 'username:password'
    getUrl()    // Builds the first part of the URL to connect to the bitbucket api

#### Prototype methods

    user();

Returns a `User` object

    getRepository(repository, cb)

Takes an `repository` parameter with the `owner` and `slug` keys for the repository to get. To get details for the repository of this module the owner should be `hgarcia` and the slug `node-bitbucket-api`.

    repositories(cb)

Result is an array of the repositories the user has access to.


### Repository

#### Instance properties

This are mapped from the api result into a more proper JavaScript format, mostly replacing '_' with camelcase names.

    provider // A reference to the BitBucket instance that created it.
    scm // String either 'git' or 'mercurial'
    hasWiki // Boolean
    lastUpdated // Date
    creator // String username of the person who created the repository
    forksCount // Number
    createdOn // Date
    owner // String username of the repository owner
    logo // String || null
    emailMailingList //String
    isMq // Boolean
    size // Number
    readOnly // Boolean
    forkOff // String || null
    mqOf // String || null;
    followersCount // Number
    state // String
    utcCreatedOn // Date
    website // String
    description // String
    hasIssuses // Boolean
    isFork // Boolean
    slug // String
    isPrivate // Boolean
    name // String
    language // String ex: 'javascript'
    utcLastUpdated // Date
    emailWriters // Boolean
    noPublicForks // Boolean
    resourceURI // String

#### Instance methods

    credentials() // delegates to this.provider.credentials()
    getUrl()    // Builds the URL to connect to this repository

#### Prototype methods

    changeset(hash)

Returns a `Changeset` object

    changesets()

Return a `Changesets` object

    events()

Return an `Events` object

    components()

Returns a `Components` object

    followers(cb)

Returns a list of followers.

    issues()

Returns an `Issues` object

    issue(issueId)

Takes the `id` of an issue and returns an `Issue` object.

    milestones()

Returns a `Milestones` object

    sources(path, revision)

Takes the path to a folder (defaults to "/") or file and optionally the revision or branch name (defaults to master). Returns a `Sources` object.
If `path` ends on "/" the API considers that you are looking for a directory instead of a file.

    versions()

Returns a `Versions` object.


### Changesets

Provides access to the changesets for a repository

#### Instance methods

    get(limit, start, cb)

Returns a list of changesets starting from the `start` hash. The `limit` is an integer indicating how many to return.

    getById(hash, cb)

Takes the `hash` for a given changeset and returns it.

### Events

Provides access to the changesets for a repository

#### Instance methods

    get(limit, start, cb)

Returns a list of events starting from the `start` index (set to `0` to get events from the creation of the repository). The `limit` is an integer indicating how many to return.

### Changeset

Provides access to stat, diff, likes and comments on a given changeset.

#### Instance methods

    getDiff(cb)

Get the diff of the changeset.

    getStats(cb)

Get statistics for the given cahngeset.

    getLikes(cb)

Get the likes for the changeset

    comments()

Returns a `Changeset.Comments` object

### Changeset.Comments

Standard CRUD API.
The `obj` argument is a literal representing a changeset comment.

### Components, Milestones, Versions

Use the CRUD API. The `getAll` method takes only a callback.
The `obj` parameter is a string.

### Issue

Provides access to the comments on an issue

#### Instance methods

    comments()

Return an `Issue.Comments` object

### Issue.Comments

Standard CRUD API.
The `obj` argument is a string.

### Issues

Custom CRUD API.

Like the standard CRUD API but without instead of a `getAll` method exposes a `get` method that takes an `options` argument and a callback. The `options` is used to filter the issues returned.

The `obj` argument is a literal that represents the issue object.

### Sources

Provides access to information about files and directories in the repository as well as raw content from a file or folder structure.

#### Instance methods

    info(cb)
    raw(cb)

The result contains a data and a lines properties. This only works for text files.

    result.data //Contains a string with the file raw content
    result.lines //Contains an array with the string split by '\n' characters.

### User

#### Instance methods

    follows(cb)

List of repositories that the user follows.

    get(cb)

Details of the user account

    privileges(cb)

List of user privileges

    update(id, obj, cb)

Update the user details, `obj` is a literal with the data to change.

    repositories()

Return a `User.Repositories` object.

### User.Repositories

#### Instance methods

    dashboard(cb)

The data show in a user dashboard.

    following(cb)

Info about repositories the user follow.

    getAll(cb)

List of all repositories for the user.


### Users