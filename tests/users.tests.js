var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;
var emailAddresses = require('./helper').emails;

describe('BitBucket.users()', function() {
  var client;
  var account;
  var emails;
  var invitations;
  var oauth;
  var privileges;
  var sshKeys;
  var oauth_id;
  var sshKey_id;
  before(function (done) {
    client = bitbucket.createClient(goodOptions);
    account = client.users(repoData.owner).account();
    emails = client.users(repoData.owner).emails();
    invitations = client.users(repoData.owner).invitations();
    oauth = client.users(repoData.owner).oauth();
    privileges = client.users(repoData.owner).privileges();
    sshKeys = client.users(repoData.owner).sshKeys();
    done();
  });

  describe(".account().get()", function () {
    it('should return information about the account for the user', function (done) {
      account.get(function (err, result) {
        result.should.have.property('repositories');
        result.should.have.property('user');
        done(err);
      });
    });
  });

  describe(".account().plan()", function () {
    it('should return the number of users in the plan', function (done) {
      account.plan(function (err, result) {
        result.should.have.property("count");
        done(err);
      });
    });
  });

  describe(".account().followers()", function () {
    it('should return the followers of the account', function (done) {
      account.followers(function (err, result) {
        result.should.have.property("followers");
        result.should.have.property("count");
        done(err);
      });
    });
  });

  describe(".emails().getAll()", function () {
    it('should return a list of emails', function (done) {
      emails.getAll(function (err, result) {
        Array.isArray(result).should.be.ok;
        done(err);
      });
    });
  });

  describe(".emails().get(email)", function () {
    it('should return details for one email', function (done) {
      emails.get(emailAddresses.accountEmail, function (err, result) {
        result.should.have.property('active');
        result.should.have.property('primary');
        result.should.have.property('email');
        done(err);
      });
    });
  });

  describe(".emails().post(email)", function () {
    it('should add a new email to the account', function (done) {
      emails.add(emailAddresses.newEmail, function (err, result) {
        result.should.have.property('active');
        result.should.have.property('primary');
        result.should.have.property('email');
        done(err);
      });
    });
  });

  describe(".emails().setAsPrimary(email)", function () {
    it('should set this email as the primary', function (done) {
      emails.setAsPrimary(emailAddresses.newEmail, function (err, result) {
        result.should.have.property('active');
        done(err);
      });
    });
  });

  describe(".invitations().add(options, cb)", function () {
    it('should send an invitation', function (done) {
      var options = {
        email_address: "hernan@mail.com",
        group_owner: "inline",
        group_slug: "developers"
      };
      invitations.add(options, function (err, result) {
        result.raw.should.eql('OK');
        done(err);
      });
    });
  });

  describe(".invitations().get(options, cb)", function () {
    it('should get a list of pernding invitations for that email', function (done) {
      invitations.get("hernan@mail.com", function (err, result) {
        result.should.have.property('groups');
        result.should.have.property('invited_by');
        result.should.have.property('email');
        result.email.should.eql("hernan@mail.com");
        done(err);
      });
    });
  });

  describe(".invitations().get(options, cb)", function () {
    it('should get a list of pernding invitations for that email for that group', function (done) {
      var options = {
        email_address: "hernan@mail.com",
        group_owner: "inline",
        group_slug: "developers"
      };
      invitations.get(options, function (err, result) {
        result.raw.should.eql('OK');
        done(err);
      });
    });
  });

  describe(".invitations().remove(options, cb)", function () {
    it('should remove the invitation for the account', function (done) {
      invitations.remove("hernan@mail.com", function (err, result) {
        (result === null).should.be.ok;
        done(err);
      });
    });
  });

  describe(".oauth().create(options, cb)", function () {
    it('should create a new oauth in the system', function (done) {
      var oauthObj = {name: 'new from api', description: 'Created by tests', url: 'http://test.com'};
      oauth.create(oauthObj, function (err, result) {
        result.name.should.eql(oauthObj.name);
        result.description.should.eql(oauthObj.description);
        oauth_id = result.id;
        done(err);
      });
    });
  });

  describe(".oauth().getAll(cb)", function () {
    it('should return all existing oauth in the system', function (done) {
      oauth.getAll(function (err, result) {
        Array.isArray(result).should.be.ok;
        done(err);
      });
    });
  });

  describe(".oauth().update(id, options, cb)", function () {
    it('should update the oauth sytem', function (done) {
      var newValues = {description: 'Updated', name: 'Api updated'};
      oauth.update(oauth_id, newValues, function (err, result) {
        result.description.should.eql(newValues.description);
        result.name.should.eql(newValues.name);
        done(err);
      });
    });
  });

  describe(".oauth().remove(id, cb)", function () {
    it('should remove the oauth system', function (done) {
      oauth.remove(oauth_id, function (err, result) {
        (result === null).should.be.ok;
        done(err);
      });
    });
  });

  describe(".privileges().getAll(cb)", function () {
    it('should return all privileges', function (done) {
      privileges.getAll(function (err, result) {
        Object.keys(result).length.should.not.eql(0);
        done(err);
      });
    });
  });

  describe(".privileges().getAll(options, cb)", function () {
    it('should return all privileges', function (done) {
      privileges.getAll({owner: 'inline', group_slug: 'developers'}, function (err, result) {
        result.should.have.property('privilege');
        done(err);
      });
    });
  });

  describe(".sshKeys().getAll(cb)", function () {
    it('should return all existing ssh keys', function (done) {
      sshKeys.getAll(function (err, result) {
        Array.isArray(result).should.be.ok;
        done(err);
      });
    });
  });

});
