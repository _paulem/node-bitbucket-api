var should = require("should");
var bitbucket = require('../index');
var goodOptions = require('./helper').credentials;
var repoData = require('./helper').repository;
var issue = {title: "New Issue", content: "Issue content"};
var component = "First component";

describe('Repository', function() {
  var repo;

  before(function (done) {
    var client = bitbucket.createClient(goodOptions);
    client.getRepository(repoData, function (err, repository) {
      if (err) {return done(err);}
      repo = repository;
      done();
    });
  });

  describe(".components()", function () {
    var componentId = null;

    describe(".create()", function () {
      it('should create a new component', function (done) {
        repo.components().create(component, function (err, result) {
          componentId = result.id;
          result.name.should.eql(component);
          done(err);
        });
      });
    });

    describe(".getById()", function () {
      it('should get the component by the id', function (done) {
        repo.components().getById(componentId, function (err, result) {
          result.name.should.eql(component);
          result.id.should.eql(componentId);
          done(err);
        });
      });
    });

    describe(".getAll()", function () {
      it('should get all components', function (done) {
        repo.components().getAll(function (err, result) {
          Array.isArray(result).should.be.ok;
          done(err);
        });
      });
    });

    describe(".update()", function () {
      it('should update a component', function (done) {
        repo.components().update(componentId, "Updated content", function (err, result) {
          result.name.should.eql("Updated content");
          done(err);
        });
      });
    });

    describe(".remove()", function () {
      it('should remove a given component', function (done) {
        repo.components().remove(componentId, function (err, result) {
          (result === null).should.be.ok;
          done(err);
        });
      });
    });

    after(function(done) {
      repo.components().getAll(function (err, components) {
        if (components.length === 0) { return done(); }
        components.forEach(function (m) {
          repo.components().remove(m.id, function (err, result) {
            done(err);
          });
        })
      });
    });

  });
});
